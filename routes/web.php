<?php

use Bittacora\Bpanel4\BlocksGrid\Http\Controllers\BlocksGridAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/rejilla-de-bloques/')->middleware(['web', 'auth', 'admin-menu'])
    ->name('bpanel4-blocks-grid.admin.')->group(function (): void {
        Route::get('', [BlocksGridAdminController::class, 'index'])->name('index');
        Route::get('create', [BlocksGridAdminController::class, 'create'])->name('create');
        Route::post('store', [BlocksGridAdminController::class, 'store'])->name('store');
        Route::get('/{grid}/edit', [BlocksGridAdminController::class, 'edit'])->name('edit');
        Route::post('/{grid}/update', [BlocksGridAdminController::class, 'update'])->name('update');
        Route::delete('/{grid}/destroy', [BlocksGridAdminController::class, 'destroy'])->name('destroy');
    });
