<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Tests\Unit;

use Bittacora\Bpanel4\BlocksGrid\Actions\CreateGrid;
use Bittacora\Bpanel4\BlocksGrid\Dtos\GridDto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class CreateGridTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    private CreateGrid $createGrid;

    public function setUp(): void
    {
        parent::setUp();

        $this->createGrid = new CreateGrid();
    }

    public function testCreaUnaRejilla(): void
    {
        $name = $this->faker->name();

        $gridDto = new GridDto($name);
        $this->createGrid->execute($gridDto);

        $this->assertDatabaseHas('block_grids', ['name' => $name]);
    }
}