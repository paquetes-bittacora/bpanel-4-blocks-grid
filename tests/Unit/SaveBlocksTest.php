<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Tests\Unit;

use Bittacora\Bpanel4\BlocksGrid\Actions\SaveBlocks;
use Bittacora\Bpanel4\BlocksGrid\Dtos\BlockDto;
use Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class SaveBlocksTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private BlockGrid $blockGrid;
    private SaveBlocks $saveBlocks;

    protected function setUp(): void
    {
        parent::setUp();

        $this->saveBlocks = $this->app->make(SaveBlocks::class);
        $this->blockGrid = $this->createBlockGrid();
    }

    public function testGuardaLosBloquesDeUnaRejilla(): void
    {
        $this->assertDatabaseCount('block_grids_blocks', 0);
        $this->saveBlocks->execute($this->blockGrid, $this->getBlockDtos());
        $this->assertDatabaseCount('block_grids_blocks', 3);
    }

    /**
     * @return BlockDto[]
     */
    private function getBlockDtos(): array
    {
        $output = [];

        for ($i = 0; $i < 3; ++$i) {
            $output[] = new BlockDto(
                title: $this->faker->name,
                rows: $this->faker->numberBetween(1, 4),
                columns: $this->faker->numberBetween(1, 4),
                description: $this->faker->text,
                backgroundUrl: $this->faker->url,
                linkUrl: $this->faker->url,
                newTab: $this->faker->boolean,
            );
        }

        return $output;
    }

    private function createBlockGrid(): BlockGrid
    {
        $blockGrid = new BlockGrid();
        $blockGrid->setName($this->faker->name);
        $blockGrid->save();
        return $blockGrid;
    }
}
