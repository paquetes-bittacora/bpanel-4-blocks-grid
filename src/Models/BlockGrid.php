<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|BlockGrid newModelQuery()
 * @method static Builder|BlockGrid newQuery()
 * @method static Builder|BlockGrid query()
 * @method static Builder|BlockGrid whereCreatedAt($value)
 * @method static Builder|BlockGrid whereId($value)
 * @method static Builder|BlockGrid whereName($value)
 * @method static Builder|BlockGrid whereUpdatedAt($value)
 * @method Builder|BlockGrid when($value = null, callable $callback = null, callable $default = null)
 * @method Builder|BlockGrid orderBy($column, $direction = 'asc')
 * @mixin Model
 */
final class BlockGrid extends Model
{
    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return HasMany<BlockGridBlock>
     */
    public function blocks(): HasMany
    {
        return $this->hasMany(BlockGridBlock::class, 'block_grid_id', 'id');
    }
}
