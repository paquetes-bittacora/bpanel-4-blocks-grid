<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Bittacora\Bpanel4\BlocksGrid\Models\BlockGridBlock
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $link_url
 * @property int|null $new_tab
 * @property string|null $background_url
 * @property int $rows
 * @property int $columns
 * @property string $locale
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read BlockGrid|null $grid
 * @method static Builder|BlockGridBlock newModelQuery()
 * @method static Builder|BlockGridBlock newQuery()
 * @method static Builder|BlockGridBlock query()
 * @method static Builder|BlockGridBlock whereBackgroundUrl($value)
 * @method static Builder|BlockGridBlock whereColumns($value)
 * @method static Builder|BlockGridBlock whereCreatedAt($value)
 * @method static Builder|BlockGridBlock whereDescription($value)
 * @method static Builder|BlockGridBlock whereId($value)
 * @method static Builder|BlockGridBlock whereLinkUrl($value)
 * @method static Builder|BlockGridBlock whereLocale($value)
 * @method static Builder|BlockGridBlock whereNewTab($value)
 * @method static Builder|BlockGridBlock whereRows($value)
 * @method static Builder|BlockGridBlock whereTitle($value)
 * @method static Builder|BlockGridBlock whereUpdatedAt($value)
 * @property int $block_grid_id
 * @property string $css_classes
 * @property int $block_order
 * @method static Builder|BlockGridBlock whereBlockGridId($value)
 * @mixin Eloquent
 */
final class BlockGridBlock extends Model
{
    /** @var string $table */
    public $table = 'block_grids_blocks';

    /**
     * @return BelongsTo<BlockGrid, BlockGridBlock>
     */
    public function grid(): BelongsTo
    {
        return $this->belongsTo(BlockGrid::class);
    }
}
