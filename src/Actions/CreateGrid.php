<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Actions;

use Bittacora\Bpanel4\BlocksGrid\Dtos\GridDto;
use Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid;

final class CreateGrid
{
    public function execute(GridDto $dto): BlockGrid
    {
        $blockGrid = new BlockGrid();
        $blockGrid->setName($dto->name);
        $blockGrid->save();

        return $blockGrid;
    }
}
