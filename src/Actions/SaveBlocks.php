<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Actions;

use Bittacora\Bpanel4\BlocksGrid\Dtos\BlockDto;
use Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid;
use Bittacora\Bpanel4\BlocksGrid\Models\BlockGridBlock;
use Illuminate\Database\DatabaseManager;
use Throwable;

final class SaveBlocks
{
    public function __construct(private readonly DatabaseManager $db)
    {
    }

    /**
     * @param array<BlockDto> $blockDtos
     * @throws Throwable
     */
    public function execute(BlockGrid $blockGrid, array $blockDtos): void
    {
        $this->db->beginTransaction();
        try {
            $blockGrid->blocks()->delete();
            $this->createBlocks($blockDtos, $blockGrid);
        } catch (Throwable $t) {
            $this->db->rollBack();
            throw $t;
        }
        $this->db->commit();
    }

    /**
     * @param BlockDto[] $blockDtos
     */
    private function createBlocks(array $blockDtos, BlockGrid $blockGrid): void
    {
        foreach ($blockDtos as $blockDto) {
            $block = new BlockGridBlock();
            $block->title = $blockDto->title;
            $block->description = $blockDto->description;
            $block->link_url = $blockDto->linkUrl;
            $block->new_tab = $blockDto->newTab ? 1 : 0;
            $block->background_url = $blockDto->backgroundUrl;
            $block->rows = $blockDto->rows;
            $block->columns = $blockDto->columns;
            $block->css_classes = $blockDto->cssClasses;
            $block->block_order = $blockDto->blockOrder;
            $block->block_grid_id = $blockGrid->getId();
            $block->save();
        }
    }
}
