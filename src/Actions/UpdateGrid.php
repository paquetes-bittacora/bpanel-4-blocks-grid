<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Actions;

use Bittacora\Bpanel4\BlocksGrid\Dtos\GridDto;
use Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid;

final class UpdateGrid
{
    public function execute(GridDto $dto): BlockGrid
    {
        $blockGrid = BlockGrid::whereId($dto->id)->firstOrFail();
        $blockGrid->setName($dto->name);
        $blockGrid->save();

        return $blockGrid;
    }
}
