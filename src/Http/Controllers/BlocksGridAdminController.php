<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\BlocksGrid\Actions\CreateGrid;
use Bittacora\Bpanel4\BlocksGrid\Actions\SaveBlocks;
use Bittacora\Bpanel4\BlocksGrid\Actions\UpdateGrid;
use Bittacora\Bpanel4\BlocksGrid\Dtos\BlockDto;
use Bittacora\Bpanel4\BlocksGrid\Dtos\GridDto;
use Bittacora\Bpanel4\BlocksGrid\Http\Requests\StoreGridRequest;
use Bittacora\Bpanel4\BlocksGrid\Http\Requests\UpdateGridRequest;
use Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Translation\Translator;
use Throwable;

final class BlocksGridAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $urlGenerator,
        private readonly ExceptionHandler $exceptionHandler,
        private readonly Redirector $redirector,
        private readonly DatabaseManager $db,
        private readonly Translator $translator,
    ) {
    }

    public function index(): View
    {
        return $this->view->make('bpanel4-blocks-grid::bpanel.index');
    }

    public function create(): View
    {
        return $this->view->make('bpanel4-blocks-grid::bpanel.create', [
            'panelTitle' => $this->translator->get('bpanel4-blocks-grid::bpanel.create'),
            'action' => $this->urlGenerator->route('bpanel4-blocks-grid.admin.store'),
            'grid' => null,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function store(StoreGridRequest $request, CreateGrid $createGrid, SaveBlocks $saveBlocks): RedirectResponse
    {
        try {
            $grid = $createGrid->execute(GridDto::fromArray($request->validated()));
            $blockDtos = $this->getBlockDtosFromRequest($request);
            $saveBlocks->execute($grid, $blockDtos);
            return $this->redirector->route('bpanel4-blocks-grid.admin.index')->with([
                'alert-success' => $this->translator->get('bpanel4-blocks-grid::bpanel.grid-created'),
            ]);
        } catch (Throwable $e) {
            $this->exceptionHandler->report($e);
            return $this->redirector->back()->with([
                'alert-danger' => $this->translator->get('bpanel4-blocks-grid::bpanel.grid-not-created'),
            ]);
        }
    }

    public function edit(BlockGrid $grid): View
    {
        return $this->view->make('bpanel4-blocks-grid::bpanel.edit', [
            'panelTitle' => $this->translator->get('bpanel4-blocks-grid::bpanel.edit'),
            'action' => $this->urlGenerator->route('bpanel4-blocks-grid.admin.update', ['grid' => $grid]),
            'grid' => $grid,
        ]);
    }

    /**
     * @throws Throwable
     */
    public function update(UpdateGridRequest $request, UpdateGrid $updateGrid, SaveBlocks $saveBlocks): RedirectResponse
    {
        try {
            $grid = $updateGrid->execute(GridDto::fromArray($request->validated()));
            $blockDtos = $this->getBlockDtosFromRequest($request);
            $saveBlocks->execute($grid, $blockDtos);
            return $this->redirector->route('bpanel4-blocks-grid.admin.index')->with([
                'alert-success' => $this->translator->get('bpanel4-blocks-grid::bpanel.grid-updated'),
            ]);
        } catch (Throwable $e) {
            $this->exceptionHandler->report($e);
            return $this->redirector->back()->with([
                'alert-danger' => $this->translator->get('bpanel4-blocks-grid::bpanel.grid-not-updated'),
            ]);
        }
    }

    /**
     * @return BlockDto[]
     */
    private function getBlockDtosFromRequest(FormRequest $request): array
    {
        $output = [];

        if (!$request->has('blocks')) {
            return $output;
        }

        /** @var array<array<string>> $requestBlocks */
        $requestBlocks = $request->get('blocks');
        foreach ($requestBlocks as $requestBlock) {
            $output[] = new BlockDto(
                title: $requestBlock['title'],
                rows: (int) $requestBlock['rows'],
                columns: (int) $requestBlock['columns'],
                description: $requestBlock['description'],
                backgroundUrl: $requestBlock['background_url'],
                linkUrl: $requestBlock['link_url'],
                newTab: isset($requestBlock['new_tab']) && 1 === (int) $requestBlock['new_tab'],
                cssClasses: $requestBlock['css_classes'] ?? '',
                blockOrder: (int) $requestBlock['block_order'],
            );
        }

        return $output;
    }

    /**
     * @throws Throwable
     */
    public function destroy(BlockGrid $grid): RedirectResponse
    {
        try {
            $this->db->beginTransaction();
            $grid->blocks()->delete();
            $grid->delete();
            $this->db->commit();
            return $this->redirector->back()->with(['alert-success' => $this->translator->get('bpanel4-blocks-grid::bpanel.grid-deleted')]);
        } catch (Throwable $exception) {
            $this->exceptionHandler->report($exception);
            $this->db->rollBack();
            return $this->redirector->back()->with([
                'alert-success' => $this->translator->get('bpanel4-blocks-grid::bpanel.grid-not-deleted'),
            ]);
        }
    }
}
