<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Http\Requests;

use Illuminate\Auth\AuthManager;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @method array<string, string> validated($key = null, $default = null)
 */
final class UpdateGridRequest extends FormRequest
{
    public function authorize(AuthManager $auth): bool
    {
        return $auth->user()?->can('bpanel4-blocks-grid.admin.store') ?? false;
    }

    /**
     * @return array{id: string, name: string}
     */
    public function rules(): array
    {
        return [
            'id' => 'required|exists:block_grids,id',
            'name' => 'required|string',
        ];
    }
}
