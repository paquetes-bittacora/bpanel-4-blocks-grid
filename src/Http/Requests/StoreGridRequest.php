<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Http\Requests;

use Illuminate\Auth\AuthManager;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @method array<string, string> validated($key = null, $default = null)
 */
final class StoreGridRequest extends FormRequest
{
    public function authorize(AuthManager $auth): bool
    {
        return $auth->user()?->can('bpanel4-blocks-grid.admin.store') ?? false;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
        ];
    }
}