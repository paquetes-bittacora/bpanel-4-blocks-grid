<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Http\Livewire;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;
use RuntimeException;

final class BlocksGridEditor extends Component
{
    use WithFileUploads;
    use WithFileUploads {
        finishUpload as traitFinishUpload;
    }

    /**
     * @var array<array<string|int|bool|null|TemporaryUploadedFile>>
     */
    public array $blocks = [];

    private ?BackgroundImageUploader $backgroundImageUploader = null;
    private ?Factory $view = null;

    public function boot(
        BackgroundImageUploader $backgroundImageUploader,
        Factory $view,
    ): void {
        $this->backgroundImageUploader = $backgroundImageUploader;
        $this->view = $view;
    }

    /**
     * @phpstan-assert BackgroundImageUploader $this->backgroundImageUploader
     */
    public function checkUploaderType(): void
    {
        if (!$this->backgroundImageUploader instanceof BackgroundImageUploader) {
            throw new RuntimeException();
        }
    }

    public function render(): View
    {
        if (!$this->view instanceof Factory) {
            throw new RuntimeException();
        }

        usort($this->blocks, static function (array $blockA, array $blockB) {
            return $blockA['block_order'] <=> $blockB['block_order'];
        });
        return $this->view->make('bpanel4-blocks-grid::bpanel.livewire.blocks-grid-editor');
    }

    public function addBlock(): void
    {
        $this->blocks[] = [
            'rows' => 1,
            'columns' => 1,
            'title' => 'Título del bloque',
            'description' => 'Descripción del bloque',
            'background' => null,
            'background_url' => null,
            'link_url' => null,
            'new_tab' => false,
            'css_classes' => '',
            'block_order' => count($this->blocks),
        ];
    }

    public function removeBackground(int $index): void
    {
        $this->blocks[$index]['background'] = null;
        $this->blocks[$index]['background_url'] = null;
    }

    /**
     * @param array<string> $tmpPath
     */
    public function finishUpload(string $name, array $tmpPath, bool $isMultiple): void
    {
        $this->checkUploaderType();

        $this->traitFinishUpload($name, $tmpPath, $isMultiple);
        $index = (int)explode('.', $name)[1];
        if (is_object($this->blocks[$index]['background']) && is_a($this->blocks[$index]['background'], TemporaryUploadedFile::class)) {
            $this->blocks[$index]['background_url'] = $this->backgroundImageUploader->uploadFile(
                $this->blocks[$index]['background']->getRealPath(),
                $tmpPath[0]
            );
        }
    }

    public function moveUp(int $index): void
    {
        if (!is_int($this->blocks[$index + 1]['block_order']) || !is_int($this->blocks[$index]['block_order'])) {
            return;
        }

        --$this->blocks[$index + 1]['block_order'];
        ++$this->blocks[$index]['block_order'];
    }

    public function moveDown(int $index): void
    {
        if (!is_int($this->blocks[$index - 1]['block_order']) || !is_int($this->blocks[$index]['block_order'])) {
            return;
        }

        ++$this->blocks[$index - 1]['block_order'];
        --$this->blocks[$index]['block_order'];
    }

    public function removeBlock(int $index): void
    {
        unset($this->blocks[$index]);

        $order = 0;
        foreach ($this->blocks as &$block) {
            $block['block_order'] = $order++;
        }
    }
}
