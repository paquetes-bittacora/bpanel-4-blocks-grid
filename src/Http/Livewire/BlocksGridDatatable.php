<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Http\Livewire;

use Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use RuntimeException;

final class BlocksGridDatatable extends DataTableComponent
{
    public function rowView(): string
    {
        return 'bpanel4-blocks-grid::bpanel.livewire.blocks-grid-datatable';
    }

    /**
     * @return Column[]
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name'),
            Column::make('Acciones', 'id')->view('bpanel4-blocks-grid::bpanel.livewire.datatable-columns.actions'),
        ];
    }

    public function query(): Builder
    {
        $query = BlockGrid::query()->orderBy('name', 'DESC')->
        when($this->getAppliedFilterWithValue('search'), fn (Builder $query, int|string|null $term): Builder => $query->where('name->es', 'like', '%'.strtoupper((string) $term).'%')
            ->orWhere('name->es', 'like', '%'.strtolower((string) $term).'%')->orWhere('name->es', 'like', '%'.ucfirst((string) $term).'%'));


        if (!is_a($query, Builder::class)) {
            throw new RuntimeException();
        }

        return $query;
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
