<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Http\Livewire;

use Illuminate\Filesystem\FilesystemManager;
use RuntimeException;

final class BackgroundImageUploader
{
    public function __construct(private readonly FilesystemManager $storage)
    {
    }

    public function uploadFile(string $path, string $tempFilePath): string
    {
        $uploadPath = ltrim($tempFilePath, DIRECTORY_SEPARATOR);
        $contents = file_get_contents($path);

        if (false === $contents) {
            throw new RuntimeException('No se pudo leer el archivo');
        }

        $this->storage->disk('images')->put($uploadPath, $contents);
        return $this->storage->disk('images')->getConfig()['url'] . '/' . $uploadPath;
    }
}
