<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Dtos;

use Bittacora\Dtos\Dto;

final class GridDto extends Dto
{
    public function __construct(
        public readonly string $name,
        public readonly ?int $id = null,
    ) {
    }
}