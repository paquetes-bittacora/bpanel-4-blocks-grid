<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Dtos;

use Bittacora\Dtos\Dto;

final class BlockDto extends Dto
{
    public function __construct(
        public readonly string $title,
        public readonly int $rows,
        public readonly int $columns,
        public readonly ?string $description = null,
        public readonly ?string $backgroundUrl = null,
        public readonly ?string $linkUrl = null,
        public readonly bool $newTab = false,
        public readonly string $cssClasses = '',
        public readonly int $blockOrder = 0,
    ) {
    }
}
