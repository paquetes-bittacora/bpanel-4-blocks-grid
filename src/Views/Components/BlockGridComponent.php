<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Views\Components;

use Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

final class BlockGridComponent extends Component
{
    public function __construct(
        private readonly Factory $view,
        public readonly BlockGrid $grid,
        public readonly string $class = '',
    ) {
    }

    public function render(): View
    {
        return $this->view->make('bpanel4-blocks-grid::components.grid');
    }
}
