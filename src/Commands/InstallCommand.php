<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string */
    protected $signature = 'blocks-grid:install';

    /** @var string|null */
    protected $description = 'Instala el paquete para crear rejillas de bloques.';

    /**
     * @var string[]
     */
    private const PERMISSIONS = [
        'index',
        'create',
        'show',
        'edit',
        'destroy',
        'store',
        'update',
    ];


    public function handle(AdminMenu $adminMenu): void
    {
        $this->comment('Instalando el módulo Bpanel4 Blocks Grid...');

        $this->createMenuEntries($adminMenu);
        $this->createTabs();

        $this->giveAdminPermissions();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-blocks-grid.admin.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'bpanel4-blocks-grid.admin.index',
            'bpanel4-blocks-grid.admin.index',
            'bpanel4-blocks-grid.admin.index',
            'Listado',
            'fa fa-list'
        );
        Tabs::createItem(
            'bpanel4-blocks-grid.admin.index',
            'bpanel4-blocks-grid.admin.create',
            'bpanel4-blocks-grid.admin.create',
            'Nueva',
            'fa fa-plus'
        );
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('contents', 'Contenidos', 'fas fa-text');
        $adminMenu->createModule(
            'contents',
            'bpanel4-blocks-grid.admin',
            'Rejillas de bloques',
            'fas fa-th-large'
        );
        $adminMenu->createAction(
            'bpanel4-blocks-grid.admin',
            'Listado',
            'index',
            'fas fa-list'
        );
        $adminMenu->createAction(
            'bpanel4-blocks-grid.admin',
            'Nueva',
            'create',
            'fas fa-plus'
        );
    }
}
