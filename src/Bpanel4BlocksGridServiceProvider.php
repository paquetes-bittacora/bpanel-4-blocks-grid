<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\BlocksGrid;

use Bittacora\Bpanel4\BlocksGrid\Commands\InstallCommand;
use Bittacora\Bpanel4\BlocksGrid\Http\Livewire\BlocksGridDatatable;
use Bittacora\Bpanel4\BlocksGrid\Http\Livewire\BlocksGridEditor;
use Bittacora\Bpanel4\BlocksGrid\Views\Components\BlockGridComponent;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire\LivewireManager;

final class Bpanel4BlocksGridServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-blocks-grid';

    public function boot(LivewireManager $livewire, BladeCompiler $blade): void
    {
        $this->commands([InstallCommand::class]);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);

        $livewire->component(self::PACKAGE_PREFIX . '::bpanel.blocks-grid-datatable', BlocksGridDatatable::class);
        $livewire->component(self::PACKAGE_PREFIX . '::bpanel.block-grid-editor', BlocksGridEditor::class);

        $blade->component(BlockGridComponent::class, self::PACKAGE_PREFIX . '::grid');
    }
}
