<form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data" novalidate>
    @csrf
    @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-products::product.name'),
    'required'=>true, 'value' => old('name') ?? $grid?->getName() ])
    @livewire('bpanel4-blocks-grid::bpanel.block-grid-editor', ['blocks' => $grid?->blocks?->toArray() ?? []])
    <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
    @if($grid?->getId() !== null)
        <input type="hidden" name="id" value="{{ $grid->getId() }}">
    @endif

    @if(isset($language))
        <input type="hidden" name="locale" value="{{ $language }}">
    @endif
</form>
