@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-blocks-grid::bpanel.create'))

@push('styles')
    @vite('vendor/bittacora/bpanel4-blocks-grid/resources/assets/bpanel/style.scss')
@endpush

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-blocks-grid::bpanel.create') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @include('bpanel4-blocks-grid::bpanel._form')
        </div>
    </div>
@endsection
