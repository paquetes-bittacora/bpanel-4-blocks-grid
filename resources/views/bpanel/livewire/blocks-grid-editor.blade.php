<div>
    <div class="blocks-grid">
        @foreach($blocks as $index => $block)
            <div class="block" style="grid-row: span {{ $block['rows'] }}; grid-column: span {{ $block['columns'] }}; order: {{ $block['block_order'] }};">
                @if(isset($block['background']))
                    <img src="{{ $block['background']->temporaryUrl() }}" class="background-image">
                @elseif(isset($block['background_url']) && !empty($block['background_url']))
                    <img src="{{ $block['background_url'] }}" class="background-image">
                @endif
                <div><h2>{{ $block['title'] }}</h2></div>
                <div>{{ $block['description'] }}</div>
                <div class="buttons">
                    @if($block['block_order'] > 0)
                        <button type="button" wire:click="moveDown({{ $index }})">
                            <i class="fas fa-chevron-square-down fa-fw"></i>
                        </button>
                    @endif
                    @if($block['block_order'] < count($blocks) - 1 )
                        <button type="button" wire:click="moveUp({{ $index }})">
                            <i class="fas fa-chevron-square-up fa-fw"></i>
                        </button>
                    @endif
                    <a href="#edit-{{ $index }}-block"><i class="fas fa-pencil fa-fw"></i></a>
                    <button type="button" wire:click="removeBlock({{ $index }})" onclick="confirm('¿Confirma que desea eliminar el bloque?') || event.stopImmediatePropagation()">
                        <i class="fas fa-trash text-danger"></i>
                    </button>
                </div>
            </div>
            <div class="options" id="edit-{{ $index }}-block">
                <div class="close"><a href="#"><i class="fal fa-times"></i></a></div>
                <div class="container form p-4">
                    <div class="row">
                        <label class="font-bold col-12"> Título
                            <input type="text" wire:model="blocks.{{ $index }}.title" class="form-control">
                        </label>
                    </div>
                    <div class="row">
                        <label class="font-bold col-12"> Description
                            <input type="text" wire:model="blocks.{{ $index }}.description" class="form-control">
                        </label>
                    </div>
                    <div class="row">
                        <label class="font-bold col-12"> URL del enlace
                            <input type="url" wire:model="blocks.{{ $index }}.link_url" class="form-control">
                        </label>
                    </div>
                    <div class="row">
                        <label class="font-bold col-12"> Abrir enlace en nueva pestaña
                            <input type="checkbox" value="1" wire:model="blocks.{{ $index }}.new_tab" class="pull-left">
                        </label>
                    </div>
                    <div class="row">
                        <label class="font-bold col-6"><i class="fas fa-arrows-v"></i> Alto
                            <input type="number" min="1" max="4" step="1" wire:model="blocks.{{$index}}.rows"
                                   class="form-control">
                        </label>
                        <label class="font-bold col-6"><i class="far fa-arrows-h"></i> Ancho
                            <input type="number" min="1" max="4" step="1" wire:model="blocks.{{$index}}.columns"
                                   class="form-control">
                        </label>
                    </div>
                    <div class="row">
                        <label class="font-bold col-12"> Clases CSS
                            <input type="text" wire:model="blocks.{{ $index }}.css_classes" class="form-control">
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="font-bold">Imagen de fondo:</label>
                        </div>
                    </div>
                    @if(isset($block['background']))
                        <div class="col-12 py-4 background-preview">
                            <img src="{{ $block['background']->temporaryUrl() }}" class="img-fluid mb-4">
                            <button type="button" wire:click="removeBackground({{ $index }})"><i class="fal fa-times"></i></button>
                        </div>
                    @elseif(isset($block['background_url']) && !empty($block['background_url']))
                        <div class="col-12 py-4 background-preview">
                            <img src="{{ $block['background_url'] }}" class="img-fluid mb-4">
                            <button type="button" wire:click="removeBackground({{ $index }})"><i class="fal fa-times"></i></button>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <input type="file" accept="image/png, image/jpeg" wire:model="blocks.{{ $index }}.background">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-2 text-center">
                            <a href="#"><button type="button" class="btn btn-primary"><i class="fal fa-times"></i> Cerrar</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="blocks[{{ $index }}][rows]" value="{{ $block['rows'] }}">
            <input type="hidden" name="blocks[{{ $index }}][columns]" value="{{ $block['columns'] }}">
            <input type="hidden" name="blocks[{{ $index }}][title]" value="{{ $block['title'] }}">
            <input type="hidden" name="blocks[{{ $index }}][description]" value="{{ $block['description'] }}">
            <input type="hidden" name="blocks[{{ $index }}][background_url]" value="{{ $block['background_url'] }}">
            <input type="hidden" name="blocks[{{ $index }}][link_url]" value="{{ $block['link_url'] }}">
            <input type="hidden" name="blocks[{{ $index }}][new_tab]" value="{{ $block['new_tab'] }}">
            <input type="hidden" name="blocks[{{ $index }}][css_classes]" value="{{ $block['css_classes'] }}">
            <input type="hidden" name="blocks[{{ $index }}][block_order]" value="{{ $block['block_order'] }}">
        @endforeach
    </div>
    <div class="">
        <button class="btn btn-primary" type="button" wire:click="addBlock"><i class="fas fa-plus"></i> Añadir bloque
        </button>
    </div>
</div>