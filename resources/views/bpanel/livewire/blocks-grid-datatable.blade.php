<td>
    {{ $row->name }}
</td>
<td class="text-right">
    @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-blocks-grid.admin', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la rejilla?'], key('bpanel4-blocks-grid-buttons-'.$row->id))
</td>