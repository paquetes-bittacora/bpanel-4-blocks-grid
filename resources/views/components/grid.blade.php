<?php
/**
 * @var \Bittacora\Bpanel4\BlocksGrid\Models\BlockGrid $grid
 */
?>
<div class="{{ $class }} blocks-grid">
    @foreach($grid->blocks as $block)
        <div class="block {{ $block['css_classes'] }}" style="grid-row: span {{ $block['rows'] }}; grid-column: span {{ $block['columns'] }}; order: {{ $block['block_order'] }};">
            <a href="{{ $block['link_url'] ?? '#' }}" @if($block['new_tab'] === 1) target="_blank" @endif>
                @if(isset($block['background_url']) && !empty($block['background_url']))
                    <img src="{{ $block['background_url'] }}" class="background-image">
                @endif
                <div class="title"><h2>{!! $block['title'] !!}</h2></div>
                <div class="description">{!! $block['description'] !!}</div>
            </a>
        </div>
    @endforeach
</div>