<?php

declare(strict_types=1);

return [
    'index' => 'Listado de rejillas',
    'edit' => 'Editar',
    'create' => 'Nueva',
    'grid-created' => 'Rejilla creada',
    'grid-not-created' => 'Ocurrió un error al intentar crear la rejilla',
    'grid-updated' => 'Rejilla actualizada',
    'grid-not-updated' => 'Ocurrió un error al intentar actualizar la rejilla',
    'grid-deleted' => 'Rejilla eliminada',
    'grid-not-deleted' => 'Ocurrió un error al eliminar la rejilla',
];
