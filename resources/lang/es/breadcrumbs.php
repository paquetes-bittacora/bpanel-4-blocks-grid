<?php

declare(strict_types=1);

return [
    'bpanel4-blocks-grid' => 'Rejillas de bloques',
    'index' => 'Listado',
    'admin' => 'Listado'
];