<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'block_grids_blocks';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('link_url')->nullable();
            $table->boolean('new_tab')->nullable();
            $table->string('background_url')->nullable();
            $table->unsignedTinyInteger('rows');
            $table->unsignedTinyInteger('columns');
            $table->string('locale')->default(config('app.locale'));
            $table->unsignedBigInteger('block_grid_id');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
